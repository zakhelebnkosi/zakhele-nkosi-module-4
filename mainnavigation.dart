// ignore_for_file: unnecessary_const

import 'package:animation_one/farmassessments.dart';
import 'package:animation_one/home.dart';
import 'package:animation_one/userprofile.dart';
import 'package:flutter/material.dart';

class MainNavigator extends StatefulWidget {
   const MainNavigator({Key? key}) : super(key: key);

  @override
  State<MainNavigator> createState() => _MainNavigatorState();
}

class _MainNavigatorState extends State<MainNavigator> {

  int _pageIndex = 0;
  static const List<Widget> _widgetOptions = <Widget>[
    const Home(),
    const FarmAssessments(),
    const UserProfile(),
  ];

  void _onItemTapped(int index) {
    setState(() {
      _pageIndex = index;
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Center(
        child: _widgetOptions.elementAt(_pageIndex),
      ),
      bottomNavigationBar: BottomNavigationBar(
        items: const <BottomNavigationBarItem>[
          BottomNavigationBarItem(
            icon: Icon(Icons.home),
            label: 'Home',
          ),
          BottomNavigationBarItem(
            icon: Icon(Icons.home_work),
            label: 'Assessments',
          ),
          BottomNavigationBarItem(
            icon: Icon(Icons.account_circle),
            label: 'Profile',
          ),
        ],
        currentIndex: _pageIndex,
        selectedItemColor: Colors.amber[600],
        onTap: _onItemTapped,
      ),
    );
  }
}
