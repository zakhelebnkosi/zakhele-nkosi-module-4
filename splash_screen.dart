// ignore_for_file: use_key_in_widget_constructors, prefer_const_constructors, unnecessary_new, prefer_const_literals_to_create_immutables

import 'dart:async';
import 'package:animation_one/login.dart';

import 'package:flutter/material.dart';
import 'package:sleek_circular_slider/sleek_circular_slider.dart';

class SplashScreen extends StatefulWidget {
  @override
  State<StatefulWidget> createState() => StartState();
}

class StartState extends State<SplashScreen> {
  @override
  Widget build(BuildContext context) {
    return initScreen(context);
  }

  @override
  void initState() {
    super.initState();
    startTimer();
  }

  startTimer() async {
    var duration = Duration(seconds: 17);
    return new Timer(duration, route);
  }

  route() {
    Navigator.pushReplacement(
        context, MaterialPageRoute(builder: (context) => LogIn()));
  }

  initScreen(BuildContext context) {
    return Scaffold(
      body: Container(
        constraints: BoxConstraints.expand(),
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            SizedBox(
                height: 200,
                width: 200,
                child: Stack(
                  fit: StackFit.expand,
                  clipBehavior: Clip.none,
                  children: [
                    CircleAvatar(
                      backgroundImage: AssetImage("assets/Picture1.png"),
                    ),
                  ],
                )),
            SizedBox(
              height: 40,
            ),
            Text(
              "FARM",
              style: TextStyle(fontSize: 55.0, color: Colors.green),
              textAlign: TextAlign.center,
            ),
            Text(
              "ASSESSMENT TOOL",
              style: TextStyle(fontSize: 15.0, color: Colors.green),
              textAlign: TextAlign.center,
            ),
            SizedBox(
              height: 40,
            ),
            SleekCircularSlider(
              min: 0,
              max: 100,
              initialValue: 100,
              appearance: CircularSliderAppearance(
                infoProperties: InfoProperties(
                    mainLabelStyle: TextStyle(
                  color: Colors.grey,
                  fontSize: 20,
                )),
                customColors: CustomSliderColors(
                    dotColor: Colors.green,
                    progressBarColor: Colors.green,
                    shadowColor: Colors.white,
                    trackColor: Colors.white),
                spinnerDuration: 10,
                animDurationMultiplier: 10,
                animationEnabled: true,
                startAngle: 90.0,
                angleRange: 360,
              ),
            ),
            SizedBox(
              height: 20.0,
            ),
            /* Text(
              'Loading...',
              style: TextStyle(color: Colors.grey, fontSize: 20),
            ),*/
          ],
        ),
      ),
    );
  }
}
