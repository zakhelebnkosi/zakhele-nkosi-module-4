// ignore_for_file: prefer_const_constructors

import 'package:animation_one/farmassessment_page0.dart';
import 'package:flutter/material.dart';

class FarmAssessments extends StatefulWidget {
  const FarmAssessments({Key? key}) : super(key: key);

  @override
  State<FarmAssessments> createState() => _FarmAssessmentsState();
}

class _FarmAssessmentsState extends State<FarmAssessments> {
  String farmName = "Farm Welgeluk";
  String farmingEntity = "Gladco Trading cc";
  String farmName1 = "Farm Senteeko";
  String farmingEntity1 = "Solid Germination Farms (Pty) Ltd";
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text("Farm Assessments"),
        centerTitle: true,
      ),
      body: Center(
        child: Column(
          children: [
            Card(
                child: Column(
              children: [
                ListTile(
                  title: Text(farmName),
                  subtitle: Text(farmingEntity),
                ),
                ButtonTheme(
                    child: ButtonBar(
                  children: [
                    OutlinedButton.icon(
                      onPressed: () {},
                      icon: Icon(Icons.edit),
                      label: Text("Edit"),
                    ),
                    OutlinedButton.icon(
                      onPressed: () {},
                      icon: Icon(Icons.delete),
                      label: Text("Delete"),
                    ),
                    OutlinedButton.icon(
                      onPressed: () {},
                      icon: Icon(Icons.email),
                      label: Text("E-mail"),
                    ),
                  ],
                )),
              ],
            )),
            Card(
                child: Column(
              children: [
                ListTile(
                  title: Text(farmName1),
                  subtitle: Text(farmingEntity1),
                ),
                ButtonTheme(
                    child: ButtonBar(
                  children: [
                    OutlinedButton.icon(
                      onPressed: () {},
                      icon: Icon(Icons.edit),
                      label: Text("Edit"),
                    ),
                    OutlinedButton.icon(
                      onPressed: () {},
                      icon: Icon(Icons.delete),
                      label: Text("Delete"),
                    ),
                    OutlinedButton.icon(
                      onPressed: () {},
                      icon: Icon(Icons.email),
                      label: Text("E-mail"),
                    ),
                  ],
                )),
              ],
            ))
          ],
        ),
      ),
      floatingActionButton: FloatingActionButton(
          onPressed: () {
            Navigator.push(
              context,
              MaterialPageRoute(builder: (context) => FarmInformation()),
            );
          },
          child: const Icon(Icons.add)),
    );
  }
}
